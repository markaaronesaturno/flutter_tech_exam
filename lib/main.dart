import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timetable/timetable.dart';

import '/providers/user_data.dart';
import 'providers/icon_data.dart';

import '/screens/add_zeiterfassung.dart';
import '/screens/kalender.dart';
import '/screens/my_account_screen.dart';
import '/screens/pause_screen.dart';
import '/screens/visitenkarte_screen.dart';
import '/screens/zeiterfassung_screen.dart';
import 'screens/login_screen.dart';
import 'screens/visitenkarte_screen.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
            ChangeNotifierProvider(
                create: (_) => IconsDrawer(),
            ),
            ChangeNotifierProvider(
                create: (_) => UserData(),
            ),
        ],
        child: MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
                primaryColor: Colors.white,
                textTheme: ThemeData.light().textTheme.copyWith(
                    bodyText1: TextStyle(
                        fontSize: 14,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.bold
                    ),
                    bodyText2: TextStyle(
                        fontSize: 14,
                        fontFamily: 'Mullish',  
                    ),
                    caption:  TextStyle(
                        fontSize: 22,
                        fontFamily: 'Allerta Stencil',
                        fontWeight: FontWeight.bold
                    )     
                )
            ),
            routes: {
                '/' : (ctx) => LoginScreen(),
                MyAccount.routeName : (ctx) => MyAccount(),
                Visetenkarte.routeName : (ctx) => Visetenkarte(),  
                ZeiterFassung.routeName : (ctx) => ZeiterFassung(),
                AddZeiterFassung.routeName : (ctx) => AddZeiterFassung(),
                PauseScreen.routeName : (ctx) => PauseScreen(),
                KalendarScreen.routeName : (ctx) => KalendarScreen()
            },

            localizationsDelegates: [
                TimetableLocalizationsDelegate(),
            ],
        )
    );
  }
}

