import 'package:flutter/material.dart';

import '/screens/add_zeiterfassung.dart';
import '/screens/kalender.dart';
import '/widgets/drawer.dart';

class ZeiterFassung extends StatelessWidget {
    static const routeName = '/zeiterfassung';

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                backgroundColor: Colors.white,
                elevation: 0,
                toolbarHeight: 80,
                title:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        Text(
                            'Donnerstag',
                            style: TextStyle(
                                fontFamily: 'Allerta Stencil',
                                fontSize: 22
                            ),
                        ),
                    
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                                Image.asset('./assets/images/status.png'),
                                Text(
                                    '12.01.2021',
                                    style: TextStyle(
                                        fontFamily: 'Mullish',
                                        fontSize: 12,
                                        color: Colors.grey
                                    ),
                                )
                            ],
                        )
                    ],
                ),
                actions: [
                    IconButton(
                        iconSize: 50,
                        icon: Image.asset('./assets/images/calendar-button.png',),
                        color: Colors.black,
                        onPressed: () {
                             Navigator.of(context).pushNamed(KalendarScreen.routeName);
                        },
                    ),

                    IconButton(
                        iconSize: 50,
                        icon: Image.asset('./assets/images/add-button.png'),
                        color: Colors.black,
                        onPressed: () {
                            Navigator.of(context).pushNamed(AddZeiterFassung.routeName);
                            
                        },
                    ),

                ],
            ),

            drawer: MainDrawer(),

            body: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                    Container(
                        height: 80,
                        color:Colors.white,
                        width: double.infinity,
                        padding: EdgeInsets.only(top:5),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                                Container(
                                    child: Column(
                                        children: [
                                            Container(
                                                height: 50,
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                onPressed: () {
                                                // Add your onPressed code here!
                                                },
                                                child: Image.asset('./assets/images/Union.png'),
                                                backgroundColor: Color.fromRGBO(255, 65, 65, 1),
                                            ),
                                            ),
                                            SizedBox(height: 5),
                                            Text('Sa',
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 12
                                                )
                                            )
                                        ],
                                    ),
                                ),
                                Container(
                                    child: Column(
                                        children: [
                                            Container(
                                                height: 50,
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                onPressed: () {
                                                // Add your onPressed code here!
                                                },
                                                child: Image.asset('./assets/images/Union.png', height: 40,),
                                                backgroundColor: Color.fromRGBO(255, 183, 43, 1),
                                            ),
                                            ),
                                            SizedBox(height: 5),
                                            Text(
                                                'Di',
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 12
                                                )
                                            )
                                        ],
                                    ),
                                ),
                                Container(
                                    
                                    child: Column(
                                        children: [
                                            Container(
                                                height: 50,
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                onPressed: () {
                                                // Add your onPressed code here!
                                                },
                                                child: Image.asset('./assets/images/send-klein.png'),
                                                backgroundColor: Color.fromRGBO(103, 136, 255, 1),
                                            ),
                                            ),
                                            SizedBox(height: 5),
                                            Text(
                                                'Mi',
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 12
                                                )
                                            )
                                        ],
                                    ),
                                ),
                                Container(
                                    child: Column(
                                        children: [
                                            Container(
                                                height: 50,
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                onPressed: () {
                                                // Add your onPressed code here!
                                                },
                                                child: Image.asset('./assets/images/edit-klein-icon.png'),
                                                backgroundColor: Color.fromRGBO(132,101, 240, 1),
                                            ),
                                            ),
                                            SizedBox(height: 5),
                                            Text('Do',
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 12
                                                )
                                            )
                                        ],
                                    ),
                                ),
                                Container(
                                    
                                    child: Column(
                                        children: [
                                            Container(
                                                height: 50,
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                onPressed: () {
                                                // Add your onPressed code here!
                                                },
                                                child: Text('Fr'),
                                                backgroundColor: Color.fromRGBO(237, 237, 237, 1)
                                            ),
                                            ),
                                            SizedBox(height: 5),
                                            Text(
                                                'Fr',
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 12
                                                )
                                            )
                                        ],
                                    ),
                                ),
                                Container(
                                    
                                    child: Column(
                                        children: [
                                            Container(
                                                height: 50,
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                onPressed: () {
                                                // Add your onPressed code here!
                                                },
                                                child: Text('Sa'),
                                                backgroundColor:  Color.fromRGBO(237, 237, 237, 1),
                                            ),
                                            ),
                                            SizedBox(height: 5),
                                            Text(
                                                'Sa',
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 12
                                                )
                                            )
                                        ],
                                    ),
                                ),
                                Container(
                                    child: Column(
                                        children: [
                                            Container(
                                                height: 50,
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                onPressed: () {
                                                // Add your onPressed code here!
                                                },
                                                child: Text('So'),
                                                backgroundColor:  Color.fromRGBO(237, 237, 237, 1),
                                            ),
                                            ),
                                            SizedBox(height: 5),
                                            Text(
                                                'So',
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 12
                                                )
                                            )
                                        ],
                                    ),
                                ),
                            ],
                        )
                    ),  
                ],
            ),
        );
        
    }
}