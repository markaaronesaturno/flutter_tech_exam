import 'package:flutter/material.dart';

class PauseScreen extends StatelessWidget {
    static const routeName = '/pause';
    
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                backgroundColor: Color.fromRGBO(103, 136, 255, 1) ,
                toolbarHeight: 100,
                title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        Text('Pause'),
                        Text('Projekt: 1298721398'),
                    ],
                )
            ),
            body: Center(
                child: Text('wdawda')
            )    
        );
    }
}