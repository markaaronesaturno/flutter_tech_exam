import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:time_tracking_app/widgets/time_picker.dart';

import '/providers/user_data.dart';

import '/screens/pause_screen.dart';

import '/widgets/add_zeiterfassung/button.dart';
import '/widgets/add_zeiterfassung/txtKategorie.dart';


class AddZeiterFassung extends StatefulWidget {
    static const routeName = '/add-zeiterfassung';
    String ? dropDownValue;


    AddZeiterFassung({this.dropDownValue});

  @override
  _AddZeiterFassungState createState() => _AddZeiterFassungState();
}

class _AddZeiterFassungState extends State<AddZeiterFassung> {
    
    Widget cardButton (String text, String subTitle, Color color, bool logic){
        return Container(
            padding: EdgeInsets.only(top: 10, bottom: 10, left: 10),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Text(
                                text,
                                style: TextStyle(
                                    fontFamily: 'Allerta Stencil',
                                    fontSize: 22
                                )
                            ),widget.dropDownValue != null && logic ==true
                            ? 
                                Container(
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                        ),
                                        borderRadius: BorderRadius.all(Radius.circular(20)),
                                        color: Colors.black
                                    ),
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                            Align(
                                                alignment: Alignment.center,
                                                child: Text(
                                                    widget.dropDownValue as String,
                                                    style: TextStyle(
                                                        fontFamily: 'Mulish',
                                                        fontSize: 16,
                                                        color: Colors.white
                                                    ),
                                                ),
                                            ),
                                            InkWell(
                                                onTap: (){
                                                    setState(() {
                                                    widget.dropDownValue = null;
                                                    });
                                                },
                                                child: Image.asset('./assets/images/close-klein-light.png')
                                            )
                                        ],
                                    )
                                )
                            : 
                                Text(subTitle)
                            
                        ],
                    ),
                    logic == true ? 
                    Container()
                    : 
                    FloatingActionButton(
                        elevation: 0,
                        onPressed:(){},
                        child: Image.asset('./assets/images/plus-klein-light.png'),
                        backgroundColor: color
                    )
                ],
            ),
        );
    }

    

    @override
    Widget build(BuildContext context) {
        final userData = Provider.of<UserData>(context).userList;
        print(widget.dropDownValue);
        print((userData));
        return DefaultTabController(
            length: 2,
            child: Scaffold(
                backgroundColor: Color.fromRGBO(236, 236, 236, 1),
                appBar: AppBar(
                    toolbarHeight: 100,
                    leading: InkWell(
                        onTap: () => Navigator.of(context).pop(),
                        child: Image.asset(
                            './assets/images/close-icon.png',
                            width: 42, 
                            height: 42,
                        ),
                    ),
                    actions: [
                        Container(
                            padding: EdgeInsets.only(right: 20),
                            child: Image.asset(
                                './assets/images/logo.png',
                                width: 23.94, 
                                height: 39.89,
                            ),
                        )
                    ],
                    backgroundColor: Theme.of(context).primaryColor,
                    elevation: 0,
                ),
                body: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    color:Colors.white,
                    child: Container(
                        color: Colors.white,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                TabBar(
                                    indicator: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage('./assets/images/lines.png'),
                                            fit: BoxFit.cover
                                        ),
                                        border: Border(
                                            bottom: BorderSide( 
                                                color: Color.fromRGBO(132, 101, 255, 1),
                                                width: 3
                                            )
                                        )
                                    ),
                                    tabs: [
                                        Tab(
                                            child: Text(
                                                'Arbeitszeit', 
                                                style: TextStyle(  
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 16  
                                                )
                                            )
                                        ),
                                        Tab(
                                            child: InkWell( 
                                                onTap: (){ Navigator.of(context).pushNamed(PauseScreen.routeName);},
                                                child: Text(
                                                    'Pause',
                                                    style: TextStyle(  
                                                        fontFamily: 'Allerta Stencil',
                                                        fontSize: 16  
                                                    )
                                                )
                                            ),
                                        )
                                    ],
                                ),
                                SizedBox(height:25),
                                Expanded(
                                    child: TabBarView(
                                        children: [
                                            SingleChildScrollView(
                                                child: Container(
                                                    child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                            
                                                            txtKategorie(
                                                                'Kategorie', 
                                                                'Wahlen Sie bitte Kategorie aus'
                                                            ),
                                                            SizedBox(height: 32),

                                                            txtKategorie(
                                                                'Projektnummer', 
                                                                'Projektnummer hinzufügen'
                                                            ),
                                                            
                                                            SizedBox(height:34),

                                                            Container(
                                                                // padding: EdgeInsets.all(10),
                                                                child: Row(
                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                    children: [
                                                                        Column(
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                                                cardButton(
                                                                                    'Mitarbeiter', 
                                                                                    'hinzufügen oder bearbeiten', 
                                                                                    Colors.black, true
                                                                                ),
                                                                                
                                                                            ],
                                                                        ),
                                                                        
                                                                        //The value is changing but needs refresh to make it the chose one
                                                                        FloatingActionButton(
                                                                            elevation: 0,
                                                                            onPressed:(){
                                                                                showDialog<void>(
                                                                                    context: context,
                                                                                    builder: (BuildContext context) {
                                                                                        return AlertDialog(
                                                                                            title: const Text('Choose User'),
                                                                                            content: SingleChildScrollView(
                                                                                                child: ListBody(
                                                                                                    children: [
                                                                                                        DropdownButton<String>(
                                                                                                            value: widget.dropDownValue,
                                                                                                            items: userData.map<DropdownMenuItem<String>>((value) {
                                                                                                                return DropdownMenuItem<String>(
                                                                                                                    value: value,

                                                                                                                    child: Text(value),
                                                                                                                );
                                                                                                            }).toList(),
                                                                                                            hint:Text('Select a user'),

                                                                                                            onChanged:(value){
                                                                                                                setState(() {
                                                                                                                    widget.dropDownValue = value;
                                                                                                                });
                                                                                                            },            
                                                                                                            
                                                                                                        ),
                                                                                                        
                                                                                                    ],
                                                                                                ),
                                                                                            ),
                                                                                            actions: <Widget>[
                                                                                                TextButton(
                                                                                                    child: const Text('Approve'),
                                                                                                    onPressed: () {
                                                                                                        Navigator.of(context).pop();
                                                                                                    },
                                                                                                ),
                                                                                            ],
                                                                                        );
                                                                                    },
                                                                                );
                                                                            },
                                                                            child: Image.asset('./assets/images/plus-klein-light.png'),
                                                                            backgroundColor: Colors.black,
                                                                        )
                                                                    ],
                                                                ),
                                                            ),
                                                            SizedBox(height:34),
                                                            Padding(
                                                              padding: const EdgeInsets.only(left: 8.0),
                                                              child: Text(
                                                                  'Arbeitszeit',
                                                                  style: TextStyle(
                                                                      fontFamily: 'Allerta Stencil',
                                                                      fontSize: 22
                                                                  )
                                                              ),
                                                            ),
                                                            timePickerView(),

                                                            widget.dropDownValue != null 
                                                            ? 
                                                                Column(
                                                                    children: [
                                                                        cardButton (
                                                                            'Pause', 
                                                                            'hinzufügen oder bearbeiten',
                                                                            Color.fromRGBO(103, 136, 255, 1),
                                                                            false
                                                                        ),
                                                                        cardButton (
                                                                            'Wartezeit', 
                                                                            'hinzufügen oder bearbeiten',
                                                                            Color.fromRGBO(255, 183, 43, 1),
                                                                            false
                                                                        ),
                                                                        cardButton (
                                                                            'Bereitschaftszeit', 
                                                                            'hinzufügen oder bearbeiten',
                                                                            Color.fromRGBO(103, 136, 255, 1),
                                                                            false
                                                                        ),
                                                                    ],
                                                                )
                                                            : 
                                                            Container(),

                                                            SizedBox(height:34),
                                                            Container(
                                                                height: 150,
                                                                color: Color.fromRGBO(245, 245, 245, 1),
                                                                child: TextField(
                                                                    decoration: InputDecoration(
                                                                        border: InputBorder.none,
                                                                        hintText: 'Enter a search term',
                                                                        prefixIcon: Padding(
                                                                            padding: EdgeInsets.all(10),
                                                                            child: Image.asset('./assets/images/user-icon.png')
                                                                        )
                                                                    ),
                                                                )
                                                            ),
                                                            SizedBox(height:32),
                                                            Padding(
                                                              padding: const EdgeInsets.only(bottom: 40.0),
                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                                    children: [
                                                                        button(
                                                                            'Abbrechen' , 
                                                                            null, 
                                                                            Colors.white, 
                                                                            Colors.black
                                                                        ),
                                                                        button(
                                                                            'Speichern' , 
                                                                            './assets/images/send-klein.png', 
                                                                            Colors.black, 
                                                                            Colors.white
                                                                        ),
                                                                    ],
                                                                ),
                                                            ),
                                                        ],
                                                    ),
                                                ),
                                            ),
                                            Container()
                                        ] 
                                    ),
                                )
                            ],
                        ),
                    ),
                ),
            )
        );
    }
}