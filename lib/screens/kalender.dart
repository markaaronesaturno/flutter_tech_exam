import 'package:flutter/material.dart';
import 'package:timetable/timetable.dart';

class KalendarScreen extends StatelessWidget {
    static const routeName = '/kalender';

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(title: Text('Hello'),),
            body: TimetableConfig<BasicEvent>(
                // Required:
                
                eventBuilder: (context, event) => BasicEventWidget(event),
                child: MultiDateTimetable<BasicEvent>(),
                // Optional:
                
                allDayEventBuilder: (context, event, info) =>
                    BasicAllDayEventWidget(event, info: info),
                callbacks: TimetableCallbacks(
                    // onWeekTap, onDateTap, onDateBackgroundTap, onDateTimeBackgroundTap
                ),
                theme: TimetableThemeData(
                    context,
                    // startOfWeek: DateTime.monday,
                    // See the "Theming" section below for more options.
                ),
            )
        );
    }
}