import 'package:flutter/material.dart';
import 'package:time_tracking_app/screens/my_account_screen.dart';

class LoginScreen extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                        Container(
                            padding: EdgeInsets.only(top:130),
                            child: Image.asset('./assets/images/logo.png')
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 50),
                            child: Text(
                                'Flutter FieldPass', 
                                style: Theme.of(context).textTheme.bodyText1
                            )
                        ),
                        Container(
                            margin: EdgeInsets.fromLTRB(37, 56, 36, 230),
                            child: InkWell(
                                onTap: (){
                                    Navigator.of(context).pushNamed(MyAccount.routeName);
                                },
                                child: Container( 
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                width: 3, 
                                                color:Color.fromRGBO(192, 192, 192, 1)
                                            )
                                        ),
                                    ),
                                    height: 57.0,
                                    width: 302,
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                            Container(
                                                padding: EdgeInsets.only(left:15 ,right: 13),
                                                child: Image.asset('./assets/images/microsoft.png')
                                            ),
                                            Text('Sign in with Microsoft', 
                                                style: Theme.of(context).textTheme.bodyText2
                                            ),
                                            Container(
                                                padding: EdgeInsets.only(left:65 ,right: 23),
                                                child: Image.asset('./assets/images/submit.png')
                                            ),
                                        ],
                                    ),
                                ),
                            ),
                        ),
                        
                        Container(
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                    Text(
                                        'Impressum', 
                                        style: Theme.of(context).textTheme.bodyText2
                                    ),
                                    Text(
                                        'Datenschutz', 
                                        style: Theme.of(context).textTheme.bodyText2
                                    )
                                ],
                            ),
                        )
                    ],
                )
            ), 
        );
    }
}

