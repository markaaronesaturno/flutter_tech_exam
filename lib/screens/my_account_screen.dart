import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '/widgets/mein_konto/mein_konto_report.dart';
import '/widgets/user.dart';
import '/widgets/drawer.dart';

import '/providers/user_data.dart';

class MyAccount extends StatelessWidget {
    static const routeName = '/my-account';

    @override
    Widget build(BuildContext context) {
        final userData = Provider.of<UserData>(context).user;
        // print(userData[2].title);
        return Scaffold(
            appBar: AppBar(),
            drawer: MainDrawer(),
            body: Container(
                color: Color.fromRGBO(236, 236, 236, 1),
                width: double.infinity,
                padding: EdgeInsets.all(16),
                child: SingleChildScrollView(
                    child: Column(
                        children: [
                            Container(
                                padding: EdgeInsets.all(16),
                                color: Colors.white,
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                        Text(
                                            'Mein Konto', 
                                            style: TextStyle(
                                                fontFamily: 'Allerta Stencil',
                                                fontSize: 22, 
                                            )
                                        ),

                                        UserDetails(
                                            imageUrl: userData[0].imageUrl, 
                                            name: userData[0].name, 
                                            email: userData[0].email, 
                                            position: userData[0].position,
                                        ),
                                        
                                        Text(
                                            userData[2].title,
                                            style: TextStyle(
                                                fontFamily: 'Allerta Stencil',
                                                fontSize: 22, 
                                            )
                                        ),
                                        
                                        Container(
                                            padding: EdgeInsets.fromLTRB(0, 16, 0, 37),
                                            child: Row(
                                                children: [
                                                    Image.asset(userData[2].imageUrl),
                                                    Container(
                                                        padding: EdgeInsets.only(left: 16 ),
                                                        child: Column(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: [
                                                                Text(
                                                                    userData[2].name,
                                                                    style: TextStyle(
                                                                        fontFamily: 'Allerta Stencil',
                                                                        fontSize: 16
                                                                    ),
                                                                ),

                                                                RichText(
                                                                    text: TextSpan(
                                                                        text: userData[2].email,
                                                                        style: TextStyle(
                                                                            color: Colors.black54,  
                                                                            fontFamily: 'Mulish',
                                                                            fontSize: 16
                                                                        ),
                                                                        recognizer: TapGestureRecognizer()..
                                                                        onTap = () { 
                                                                            launch('https://docs.flutter.io/flutter/services/UrlLauncher-class.html'); 
                                                                        }   
                                                                    )
                                                                ),
                                                                Padding(
                                                                  padding: const EdgeInsets.only(top: 5),
                                                                  child: Image.asset('./assets/images/phone-number-icon.png'),
                                                                ),
                                                            ],
                                                        ),
                                                    )
                                                ],
                                            )
                                        ),
                
                                        MeinKontoReport( 
                                            title:'Wonchenbericht' ,
                                            text: '12.03 - 19.03.2021', 
                                            buttonText: 'Wochenbericht zuschiken'
                                        ),

                                        SizedBox(height:30),

                                        MeinKontoReport( 
                                            title:'Monatsbericht', 
                                            text: 'April 2020', 
                                            buttonText: 'Monatsberitch erstellen'
                                        ), 
                                    ],
                                ),
                            ),

                            SizedBox(height:8),
                            Container(
                                padding: EdgeInsets.only(
                                    left: 16, 
                                    right: 16
                                ),
                                height: 95,
                                color: Color.fromRGBO(224, 224, 224, 1),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                        Text(
                                            'Aktuelles budget',
                                            style: TextStyle(
                                                fontFamily: 'Mullish',
                                                fontSize: 16
                                            ),
                                        ),
                                        FloatingActionButton.extended(
                                            label: const Text('7'),
                                            onPressed: (){},
                                            backgroundColor: Color.fromRGBO(255, 183, 43, 1),
                                            
                                        )
                                    ],
                                )
                            ),

                            SizedBox(height:8),
                            Container(
                                height: 170,
                                color: Colors.white,
                                child: ListTile(
                                    title: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                            Text(
                                                'Krankheistage',
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 22
                                                ),
                                            ),
                                            SizedBox(height: 20,),
                                            Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                    Container(
                                                        child: Text(
                                                            'Insgesant',
                                                            style: TextStyle(
                                                                fontFamily: 'Mullish',
                                                                fontSize: 14
                                                            ),
                                                        )
                                                    ),
                                                    Text(
                                                        '03',
                                                        style: TextStyle(
                                                            fontFamily: 'Allerta Stencil',
                                                            fontSize: 16
                                                        ),
                                                    ),
                                                ],
                                            ),
                                            SizedBox(height: 16,),
                                            Container(
                                                height: 40,
                                                width: 190,
                                                color:Colors.black,
                                                child: Row(
                                                    children: [
                                                        TextButton(
                                                            onPressed: (){}, 
                                                            child: Text(
                                                                'Krankheit einreichen', 
                                                                style: TextStyle(color: Colors.white),
                                                            ), 
                                                        ),
                                                        Image.asset('./assets/images/plus-klein.png'),
                                                    ],
                                                ),
                                            ),
                                        ],
                                    ),
                                ),
                            ),

                            SizedBox(height:8),
                            
                            Container(
                                height: 146,
                                color: Colors.white,
                                child: ListTile(
                                    title: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                            Text(
                                                'AZ KONTO',
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 22
                                                ),
                                            ),
                                            SizedBox(height: 20,),
                                            Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                    Container(
                                                        child: Text(
                                                            'Stunden',
                                                            style: TextStyle(
                                                                fontFamily: 'Mullish',
                                                                fontSize: 14
                                                            ),
                                                        )
                                                    ),
                                                    Text(
                                                        '100 / 250',
                                                        style: TextStyle(
                                                            fontFamily: 'Allerta Stencil',
                                                            fontSize: 16
                                                        ),
                                                    )
                                                ],
                                            )
                                        ],
                                    ),
                                ),
                            ),
                        ],
                    ),
                ),
            )
        );         
    }
}

