import 'package:flutter/foundation.dart';

import 'icon_model.dart';


class IconsDrawer with ChangeNotifier{
    List<IconDrawer>  _icons = [
        IconDrawer(
            id: '1',
            title: 'Mein Konto', 
            imageUrl: './assets/images/user.png', 
            isActive: true
        ),
        IconDrawer(
            id: '2',
            title: 'Visinkarte', 
            imageUrl: './assets/images/visitenkarte-icon.png', 
            isActive: false
        ),
        IconDrawer(
            id: '3', 
            title: 'Zeiterfassung', 
            imageUrl: './assets/images/time-tracking-icon.png', 
            isActive: false
        ),
        IconDrawer(
            id: '4',
            title: 'Meine Einsätze', 
            imageUrl: './assets/images/my-stakes-icon.png', 
            isActive: false
        ),
    ];

    String _navigationid = '1';

    //Getters 
    List<IconDrawer> get icon {
        return [..._icons];
    }

    get navigationId => _navigationid;


    //Setters
    setNavigationId(navigationId){
        _navigationid = navigationId;
        notifyListeners();
    }


    

}
