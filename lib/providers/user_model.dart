class UserModel {
    final String title;
    final String imageUrl;
    final String name;
    final String email;
    final String position;

    UserModel({
        required this.title,
        required this.imageUrl,
        required this.name,
        required this.email, 
        required this.position
    });
}