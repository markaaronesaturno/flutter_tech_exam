import 'package:flutter/foundation.dart';



class IconDrawer with ChangeNotifier{
    final String id;
    final String title;
    final String imageUrl;
    final bool isActive;   

    IconDrawer({
        required this.title, 
        required this.id,
        required this.imageUrl, 
        this.isActive = false
    });
}