import 'package:flutter/foundation.dart';
import '/providers/user_model.dart';




class UserData with ChangeNotifier{
    List<UserModel> _users = [
        UserModel(
            title: 'VisitenKarte', 
            imageUrl: './assets/images/user-icon.png', 
            name: 'Greg Neu', 
            email: 'greg.neu@f-bootcamp.com', 
            position: 'Monteur'
        ),
        UserModel(
            title: 'Vorgesetzte', 
            imageUrl: './assets/images/user-icon.png', 
            name: 'Andero Mustermann', 
            email: 'andero.mustermann@f-bootcamp.com', 
            position: 'Abteilungsleiter '
        ),
        UserModel(
            title: 'Asprechpartner',
            imageUrl: './assets/images/flamingo.png', 
            name: 'Ingo Flamingo', 
            email: 'ingo.flamingo@f-bootcamp.com', 
            position: 'Abteilungsleiter '
        )
    ];

    List<UserModel> get user {
        return [..._users];
    }

    get userList {
        List user1 = [];
        for (var i = 0; i < _users.length; i++){
            user1.add(_users[i].name);
        }
        return user1; 
    }


}