import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:url_launcher/url_launcher.dart';

class UserDetails extends StatelessWidget {
  
    final String imageUrl;
    final String name;
    final String email;
    final String position;

    UserDetails({required this.imageUrl, required this.name, required this.email, required this.position});

    @override
        Widget build(BuildContext context) {
            return Container(
                padding: EdgeInsets.fromLTRB(0, 16, 0, 37),
                child: Row(
                    children: [
                        Image.asset(imageUrl),
                        Container(
                            padding: EdgeInsets.only(left: 16 ),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    Text(
                                        name,
                                        style: TextStyle(
                                            fontFamily: 'Allerta Stencil',
                                            fontSize: 16
                                        ),
                                    ),
                                    RichText(
                                        text: TextSpan(
                                            text: email,
                                            style: TextStyle(
                                                color: Colors.black54, 
                                                fontFamily: 'Mulish',
                                                fontSize: 16
                                            ),
                                            recognizer: TapGestureRecognizer()..onTap = () { launch('https://docs.flutter.io/flutter/services/UrlLauncher-class.html'); }   
                                        )
                                    ),
                                    Text(
                                        position,
                                        style: TextStyle(
                                            fontFamily: 'Mullish',
                                            fontSize: 16,
                                            color: Colors.black54,
                                        ),
                                    ),
                                ],
                            ),
                        )
                    ],
                )
            );
        }
}