import 'package:flutter/material.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';

Widget timePicker() {
    return new TimePickerSpinner(
        is24HourMode: false,
        minutesInterval: 5,
        normalTextStyle: TextStyle(
            fontFamily: 'Allerta Stencil',
            fontSize: 24,
            color: Colors.grey
        ),
        highlightedTextStyle: TextStyle(
            fontFamily: 'Allerta Stencil',
            fontSize: 24,
            color: Colors.black
        ),
        spacing: 20,
        itemHeight: 80,
        itemWidth: 35,
        isForce2Digits: true,
    );
}