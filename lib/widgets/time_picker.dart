import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'timePicker.dart';

Widget timePickerView (){
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
            Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Color.fromRGBO(76, 183, 230, 1),
                            width: 5
                        ))
                ),
                child: Row(
                    children: [
                        RotatedBox(
                            quarterTurns: 3,
                            child: Icon(FontAwesomeIcons.angleDoubleDown)
                        ),
                        timePicker(),
                    ],
                ),
            ),
        
            SizedBox(width: 10),
            
            Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Color.fromRGBO(76, 183, 230, 1),
                            width: 5
                        ))
                ),
                child: Row(
                    children: [
                        RotatedBox(
                            quarterTurns: 3,
                            child: Icon(FontAwesomeIcons.stop)
                        ),
                        timePicker(),
                    ],
                ),
            ),
            
        ],
    );
}