import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AddressAndContact extends StatelessWidget {
    final String email;

    AddressAndContact(this.email);

    @override
    Widget build(BuildContext context) {
        Widget addressAndContact(String email){
            return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Container(
                        padding: EdgeInsets.only(left: 20),
                        child: Text('Adresse', 
                        style: TextStyle(
                        fontFamily: 'Allerta Stencil',
                        fontSize: 25, 
                        )),
                    ),
                    Row(
                        children: [
                            Container(
                                padding: EdgeInsets.only(top: 24.95, left: 32),
                                child: Image.asset('./assets/images/adresse.png')
                            ),
                            Container(
                                padding: EdgeInsets.only(top: 24.95,left: 30.92),
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                        Text(
                                            'Flutter Bootcamp',
                                            style: TextStyle(
                                                fontFamily: 'Allerta Stencil',
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold
                                            )
                                        ),
                                        Text(
                                            '6783 Ayala Ave',
                                            style: TextStyle(
                                                fontFamily: 'Mulish',
                                                fontSize: 16
                                            )
                                        ),
                                        Text('1200 Metro Manila'),
                                    ],
                                )
                            )
                        ],
                    ),
                    SizedBox(height: 45,),
                    Container(
                        padding: EdgeInsets.only(left: 20),
                        child: Text('Konakt', 
                            style: TextStyle(
                                fontFamily: 'Allerta Stencil',
                                fontSize: 25, 
                            )
                        ),
                    ),
                    Row(
                        children: [
                            Container(
                                padding: EdgeInsets.only(top: 24.95, left: 32),
                                child: Image.asset('./assets/images/contact-icon.png')
                            ),
                            Flexible(
                                child: Container(
                                    padding: EdgeInsets.only(top: 24.95,left: 30.92),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                            Text(
                                                'T: +49 1234 56 789 01', 
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 16, 
                                                )
                                            ),
                                            Text(
                                                'F: +49 1234 56 789 01-2',
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 16, 
                                                )
                                            ),
                                            Text(
                                                'M: +49 1234 56',
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 16, 
                                                )
                                            ),
                                            Text(
                                                email, 
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontFamily: 'Allerta Stencil',
                                                    fontSize: 16, 
                                                ),
                                                
                                            ),
                                        ],
                                    )
                                ),
                            ),
                        ],
                    ),
                    SizedBox(height: 17),
                    Center(
                        child: RichText(
                            text: TextSpan(
                                text: 'www.flutter-bootcamp.com',
                                style: TextStyle(
                                    color: Colors.blue, 
                                    fontFamily: 'Mulish',
                                    fontSize: 16
                                ),
                                recognizer: TapGestureRecognizer()..onTap = () { launch('https://docs.flutter.io/flutter/services/UrlLauncher-class.html'); }   
                            )
                        ),
                    )

                ],
            );
        }
        return(
            Container(
                child: addressAndContact(email),
            )
        ); 
    }
}