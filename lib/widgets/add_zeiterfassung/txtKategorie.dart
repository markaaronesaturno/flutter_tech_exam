import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget txtKategorie(String title, String placeHolder){
    return Padding(
        padding: const EdgeInsets.only(left:8.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Text(
                    title,
                    style: TextStyle(
                        fontFamily: 'Allerta Stencil',
                        fontSize: 22
                    )
                ),
                TextFormField(
                    decoration: InputDecoration(
                        labelText: placeHolder,
                        suffixIcon: 
                        IconButton(
                            onPressed: null,
                            icon: Icon(FontAwesomeIcons.angleDoubleDown)
                        ),
                    ),
                ),
            ],
        ),
    );
}